import cv2
import pdfplumber
import numpy as np
from pdfplumber.page import Page

from module.extractor import ExtractConfig
from module.extractors import ImageExtractor


class PdfExtractor(ImageExtractor):
    def __init__(self, config: ExtractConfig) -> None:
        super().__init__(config, ['pdf'])

    def can_process(self, file: str):
        return super().can_process(file)

    def extract_data(self, file) -> dict:
        result = {}
        with pdfplumber.open(file) as pdf:
            for page in pdf.pages:
                result.update(self._extract_page(page))

        return result

    def _extract_page(self, page: Page) -> dict:
        result = {}
        x_scale, y_scale = self.config.resolution.get_scale(page.width, page.height)
        words = page.extract_words()

        for image in page.images:
            nparr = np.frombuffer(image['stream'].get_data(), np.uint8)
            cv2_image = cv2.imdecode(nparr, cv2.IMREAD_UNCHANGED)
            if cv2_image is not None:
                result.update(self._extract_from_image(cv2_image))

        for var in self.config.variables:
            if var.page is not None and var.page != page.page_number:
                continue

            coordinates = var.get_coordinates(words, x_scale, y_scale)
            if coordinates:
                cropped = page.within_bbox(coordinates)

                result[var.key] = ' '.join(w['text'] for w in cropped.extract_words())
                result[f"_debug_page_{page.page_number}_{var.key}"] = cropped.to_image()
                result[f"_debug_page_{page.page_number}_{var.key}"] = cropped.to_image()

        return result
