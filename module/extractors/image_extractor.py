import re
from typing import List, Optional
import cv2
import numpy
from PIL import Image
from pandas import notna
from pytesseract import pytesseract, Output

from module.extractor import DataExtractor, ExtractConfig


class ImageExtractor(DataExtractor):
    def __init__(self, config: ExtractConfig, file_types: List[str] = []) -> None:
        super().__init__(config, ['png', 'jpg', *file_types])

    def extract_data(self, file) -> dict:
        image = cv2.imread(file)
        return self._extract_from_image(image)

    def _extract_from_image(self, image: numpy.ndarray) -> dict:
        height, width, _ = image.shape
        x_scale, y_scale = self.config.resolution.get_scale(width, height)

        result = {}
        data = pytesseract.image_to_data(image, output_type=Output.DATAFRAME)
        words = [
            {
                'x0': d[1]['left'],
                'top': d[1]['top'],
                'x1': d[1]['left'] + d[1]['width'],
                'bottom': d[1]['top'] + d[1]['height'],
                'text': d[1]['text']
            }
            for d in data.iterrows()
            if isinstance(d[1]['text'], str)
        ]

        for var in self.config.variables:
            box = var.get_coordinates(words, x_scale, y_scale)
            cropped_image = image[int(box[1]):int(box[3]), int(box[0]):int(box[2])]
            cropped_result = pytesseract.image_to_string(cropped_image, config='-c preserve_interword_spaces=1')
            result[var.key] = re.sub('\\f|\\n[\\f$]', '', cropped_result)
            result[f"_debug_{var.key}"] = cropped_image

        return result
