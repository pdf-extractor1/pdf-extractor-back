from module import DecimalBox, BOX_KEYS


def dict_box_to_tuple(box: dict) -> DecimalBox:
    # noinspection PyTypeChecker
    return tuple(box[k] for k in BOX_KEYS)
