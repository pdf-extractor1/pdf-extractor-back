import os
import re
from abc import ABC, abstractmethod
from decimal import Decimal
from typing import *
from typing import Pattern

from pydantic import BaseModel

from module import DecimalBox
from module.util import dict_box_to_tuple

FloatBox = Tuple[float, float, float, float]
AnyBox = Tuple[Any, ...]


class AnchorConfig(BaseModel):
    location: Union[Pattern, DecimalBox]
    index: int = 0
    steps: int = 0

    def is_static(self) -> bool:
        return isinstance(self.location, tuple)

    def get_box(self, words: List[dict]) -> DecimalBox:
        if self.is_static():
            return self.location

        index = self._find_index([w['text'] for w in words])
        return dict_box_to_tuple(words[index]) if index is not None else None

    def _find_index(self, words: List[str], min_index=None, max_index=None) -> Optional[int]:
        w_index = 0
        found = ((i, w) for i, w in enumerate(words) if re.search(self.location, w))
        for index, word in found:
            if (max_index is not None and index > max_index) or (min_index is not None and index < min_index):
                continue

            if w_index == self.index:
                return index

            w_index += 1

        return None


class VariableConfig(BaseModel):
    key: str
    page: int = None
    anchors: List[AnchorConfig] = None
    offset: DecimalBox

    def scale_offset(self, x_scale: Decimal, y_scale: Decimal) -> DecimalBox:
        # noinspection PyTypeChecker
        return tuple(c * x_scale for c in self.offset[:2]) + tuple(c * y_scale for c in self.offset[2:])

    def get_coordinates(self, words: List[dict], x_scale=1.0, y_scale=1.0):
        anchor_coords = (0, 0, 0, 0)
        if self.anchors:
            boxes = [b for b in (a.get_box(words) for a in self.anchors) if b]
            if boxes:
                anchor_coords = tuple((
                    min(b[0] for b in boxes),
                    min(b[1] for b in boxes),
                    max(b[2] for b in boxes),
                    max(b[3] for b in boxes)
                ))
            else:
                return None

        return self._get_coordinates(anchor_coords, x_scale, y_scale)

    def _get_coordinates(self, anchor_coords: AnyBox, x_scale=1.0, y_scale=1.0) -> Tuple[Any, ...]:
        ac = anchor_coords
        x_scale = Decimal(x_scale)
        y_scale = Decimal(y_scale)
        coords = tuple(n + ac[i] for i, n in enumerate(self.scale_offset(x_scale, y_scale)))
        return coords

    def find_box(self, words: List[dict]) -> List[dict]:
        boxes = []
        texts = [w['text'] for w in words]

        min_index = 0
        max_index = len(words)
        for anchor in self.anchors:
            if anchor.is_static():
                boxes.append(anchor)
            else:
                index = anchor._find_index(texts, min_index, max_index)
                if index is None:
                    break

                boxes.append(words[index])
                min_index = index + 1
                max_index = min_index + anchor.steps

        return boxes


class ResolutionConfig(BaseModel):
    width: Decimal
    height: Decimal

    def get_scale(self, width: int, height: int) -> Tuple[Decimal, Decimal]:
        return Decimal(width) / self.width, Decimal(height) / self.height


class ExtractConfig(BaseModel):
    resolution: ResolutionConfig
    variables: List['VariableConfig']


class DataExtractor(ABC):
    config: ExtractConfig

    def __init__(self, config: ExtractConfig, file_types: List[str]) -> None:
        super().__init__()
        self.config = config
        self.file_types = file_types

    def can_process(self, file: str):
        _, extension = os.path.splitext(file)
        return extension[1:] in self.file_types

    @abstractmethod
    def extract_data(self, file: str) -> dict:
        raise NotImplementedError()
