from decimal import Decimal
from typing import List, Tuple

BOX_KEYS: List[str] = ['x0', 'top', 'x1', 'bottom']
DecimalBox = Tuple[Decimal, Decimal, Decimal, Decimal]

