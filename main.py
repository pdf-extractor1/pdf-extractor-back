import logging
import os
import pathlib

import yaml
from typing import *
from pathlib import Path

from pandas import DataFrame

from module.extractor import ExtractConfig, DataExtractor
from module.extractors import PdfExtractor, ImageExtractor

DATA_DIR = os.path.join(Path(__file__).parent, 'data')
EXTRACTORS: List[Union[DataExtractor, Type[DataExtractor]]] = [
    PdfExtractor,
]


def load_config():
    global EXTRACTORS
    with open(os.path.join(DATA_DIR, "config.yml"), 'r', encoding='utf-8') as config_file:
        config = ExtractConfig(**yaml.load(config_file, Loader=yaml.Loader))
        EXTRACTORS = [e(config) for e in EXTRACTORS]


def _get_progress(index: int, total: int) -> str:
    return f"({int(index / total * 100)}% {index}/{total})"


def extract_files():
    files_path = os.path.join(DATA_DIR, "files")

    all_files = []
    for root, directories, files in os.walk(files_path):
        if not files:
            continue

        valid_files: List[Tuple[str, DataExtractor]] = []
        for file in files:
            extractor = next((e for e in EXTRACTORS if e.can_process(file)), None)
            if extractor:
                valid_files.append((file, extractor))

        all_files.append((root, valid_files))

    total = len(all_files)
    logging.info(f"Extracting data from {total} directories")
    result = []
    for index, context in enumerate(all_files):
        root, valid_files = context
        logging.info(f"{_get_progress(index, total)} Extracting data from {root}... ({len(valid_files)} files)")

        root_path = os.path.join(files_path, root)
        for file, extractor in valid_files:
            data = extractor.extract_data(os.path.join(root_path, file))
            data = {key: value for key, value in data.items() if not key.startswith('_')}
            data.update(_source_file=file, _source_dir=root_path)
            result.append(data)

    logging.info(f"(100% {total}/{total}) All files extracted")

    # noinspection PyTypeChecker
    DataFrame.from_dict(result).to_csv(os.path.join(DATA_DIR, "result.csv"))


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logging.getLogger("pdfminer").setLevel(logging.WARNING)

    load_config()
    extract_files()
